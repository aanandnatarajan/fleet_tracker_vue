import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/assignments',
      name: 'assignments',
      component: () => import(/* webpackChunkName: "assignments" */ './views/assignments/list.vue'),
    },
    {
      path: '/assignments/new',
      name: 'assignments_new',
      component: () => import(/* webpackChunkName: "assignments_new" */ './views/assignments/new.vue'),
    },
    {
      path: '/vehicles',
      name: 'vehicles',
      component: () => import(/* webpackChunkName: "vehicles" */ './views/vehicles/list.vue'),
    },
    {
      path: '/vehicles/new',
      name: 'vehicles_new',
      component: () => import(/* webpackChunkName: "vehicles_new" */ './views/vehicles/new.vue'),
    },
    {
      path: '/vehicles/edit/:id',
      name: 'vehicles_edit',
      component: () => import(/* webpackChunkName: "vehicles_edit" */ './views/vehicles/edit.vue'),
    },
    {
      path: '/drivers',
      name: 'drivers',
      component: () => import(/* webpackChunkName: "drivers" */ './views/drivers/list.vue'),
    },
    {
      path: '/drivers/new',
      name: 'drivers_new',
      component: () => import(/* webpackChunkName: "drivers_new" */ './views/drivers/new.vue'),
    },
    {
      path: '/drivers/edit/:id',
      name: 'drivers_edit',
      component: () => import(/* webpackChunkName: "drivers_edit" */ './views/drivers/edit.vue'),
    }
  ],
});
