import Vue from 'vue';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbvue/build/css/mdb.css';
import App from './App.vue';
import router from './router';
import store from './store';
import Vuetify from 'vuetify/lib'
import VeeValidate from 'vee-validate'

import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false;

import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify);
Vue.use(VeeValidate);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
