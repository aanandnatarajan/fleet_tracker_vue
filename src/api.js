import axios from 'axios';
import NProgress from 'nprogress';

const api = axios.create({
    baseURL: 'http://139.59.92.226/api'
});

api.interceptors.request.use(config => {
    NProgress.start();
    return config;
});

api.interceptors.response.use(config => {
    NProgress.done();
    return config;
})

export default api;
